The CIA in this build was created unofficially by `SanicConnoisuer_91`. It is believed to be safe, and has been tested by both `SanicConnoisuer_91` and myself on modern, verified boot9strap and fastboot3ds setups.

TL;DR: We've tested the CIA in here the same as an official one, but don't be stupid. Create NAND backups often, and enjoy!
