# Releases
This repository exists to host our precompiled ROM images, .elf files, and .map files, as well as handy utilities for you to use. This means that we no longer have to rely on MediaFire for releases.

# Licensing

All scripts are licensed under MIT-0 See `LICENSE` for more information.

# Common issues

## How do I automatically compile under Windows?
Not sure. I don't use Windows. Your best bet is likely to be "manual" compilation using the install docs found in the `tbld/game` repository.

# Contributing
We welcome contributions of:

* buildscripts
* prebuilt CIA binaries (for the 3DS Virtual Console version)
* documentation updates and additions
* translation to other languages.

We do **not** accept any financial support *whatsoever.*

